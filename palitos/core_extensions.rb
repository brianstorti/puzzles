class Numeric
  def raiz
    return Math.sqrt(self)
  end

  def primo?
    2.upto(self.raiz + 1) do |i|
      return false if self % i == 0
    end
    return true
  end

  def possui_raiz_exata?
    return self.raiz == self.raiz.to_i
  end

  def natural?
    return (self  == self.to_i and self >= 0)
  end
end
