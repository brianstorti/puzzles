require "core_extensions"

class Contador
  def self.contar(numero)
    raise ArgumentError.new("Argumento inválido") unless numero.natural?
    return "" if numero == 0
    return "|" * numero.raiz + "X" + "|" * numero.raiz if numero.possui_raiz_exata?
    return "|" * numero if numero.primo?
    return solucao_basica(numero)
  end

  def self.solucao_basica(numero)
    div = []    
    1.upto(numero + 1) do |i|
      div << i if numero % i == 0
    end
    lado_esquerdo = div[div.size / 2]
    lado_direito = div[div.size / 2 - 1]
    return "|" * lado_esquerdo + "X" + "|" * lado_direito
  end
end
