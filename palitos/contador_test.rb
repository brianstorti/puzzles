require "test/unit"
require "contador"

class ContadorTest < Test::Unit::TestCase
  def test_retorna_string_vazia_para_zero
      assert_equal("", Contador.contar(0))
  end

  def test_retorna_multiplicacao_para_raiz_exata
    assert_equal("||||X||||", Contador.contar(16))
    assert_equal("||X||", Contador.contar(4))
  end

  def test_retorna_sem_operacao_para_numero_primo
    assert_equal("|||||", Contador.contar(5))
    assert_equal("|||||||", Contador.contar(7))
    assert_equal("|||||||||||||", Contador.contar(13))
  end

  def test_com_quize
    assert_equal("|||||X|||", Contador.contar(15))
  end

  def teste_com_seis
    assert_equal("|||X||", Contador.contar(6))
  end

  def test_excecao_lancada_para_numero_nao_natural
    assert_raise(ArgumentError) do
      Contador.contar(-1)
    end

    assert_raise(ArgumentError) do
      Contador.contar(1.2)
    end
  end
end
