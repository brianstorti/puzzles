class MiojoMaker
  def self.calculate(first_hourglass, second_hourglass, miojo_time)
    fail ArgumentError if miojo_time > first_hourglass or miojo_time > second_hourglass 
    fail RuntimeError if miojo_time % 2 != 0 and first_hourglass % 2 == 0 and second_hourglass % 2 == 0

    if first_hourglass > second_hourglass
      bigger_hourglass = first_hourglass
      smaller_hourglass = second_hourglass
    else
      bigger_hourglass = second_hourglass
      smaller_hourglass = first_hourglass
    end

    return miojo_time if miojo_time == first_hourglass or miojo_time == second_hourglass
    return bigger_hourglass if bigger_hourglass - smaller_hourglass == miojo_time

    remove_from_first = true
    left_time = bigger_hourglass - smaller_hourglass
    spent_time = smaller_hourglass
    until left_time == miojo_time do
      spent_time = spent_time + left_time
      if remove_from_first
        left_time = smaller_hourglass - left_time 
      else
        left_time = bigger_hourglass - left_time
      end
      remove_from_first = !remove_from_first 
    end

    return spent_time + miojo_time
  end
end
