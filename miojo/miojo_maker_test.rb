require "test/unit"
require "miojo_maker"

class MiojoMakerTest < Test::Unit::TestCase
  def test_raise_exception_for_invalid_arguments
    assert_raise(ArgumentError) do
      MiojoMaker.calculate(1, 1, 2)
    end
  end

  def test_raise_exception_for_impossible_solution
    assert_raise(RuntimeError) do
      MiojoMaker.calculate(2, 2, 1)
    end
  end

  def test_return_hourglass_time_if_equals_miojo_time
    assert_equal(3, MiojoMaker.calculate(8, 3, 3))
  end

  def test_return_right_time_given_perfect_time_hourglass
    assert_equal(10, MiojoMaker.calculate(10, 7, 3))
    assert_equal(10, MiojoMaker.calculate(7, 10, 3))
  end

  def test_return_right_time
    assert_equal(10, MiojoMaker.calculate(5, 7, 3))  
    assert_equal(44, MiojoMaker.calculate(22, 15, 14))
    assert_equal(150, MiojoMaker.calculate(60, 50, 30))
    assert_equal(200, MiojoMaker.calculate(199, 200, 1))
    assert_equal(40000, MiojoMaker.calculate(30000, 40000, 10000))
  end
end
