require "test/unit"
require "cash_machine"

class CashMachineTest < Test::Unit::TestCase
  def test_return_one_banknote_to_exact_value
    assert_equal([10], CashMachine.withdraw(10))
    assert_equal([20], CashMachine.withdraw(20))
    assert_equal([50], CashMachine.withdraw(50))
    assert_equal([100], CashMachine.withdraw(100))
  end

  def test_return_two_banknotes
    assert_equal([20, 10], CashMachine.withdraw(30))
    assert_equal([100, 50], CashMachine.withdraw(150)) 
  end

  def test_return_three_banknotes
    assert_equal([100, 50, 20], CashMachine.withdraw(170)) 
    assert_equal([50, 20, 10], CashMachine.withdraw(80)) 
  end

  def test_return_same_banknote_twice
    assert_equal([50, 20, 20], CashMachine.withdraw(90))
    assert_equal([100, 100, 50, 20, 20], CashMachine.withdraw(290))
  end

  def test_fail_with_impossible_value
    assert_raise(RuntimeError) do
      CashMachine.withdraw(15)
    end
  end
end
