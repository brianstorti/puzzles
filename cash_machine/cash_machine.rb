class CashMachine
  BANKNOTES = [100, 50, 20, 10]

  def self.withdraw(value)
    returned_banknotes = []
    BANKNOTES.each do |banknote|
      if banknote <= value
        value = value - banknote 
        returned_banknotes << banknote
        redo
      end
    end

    fail if value > 0

    returned_banknotes
  end
end
